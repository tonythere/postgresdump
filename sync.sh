#!/usr/bin/env bash

echo "Sync data to s3"

aws s3 sync "$OUTPUT_BASE_DIR" "$BACKUP_S3_PATH"
